//-----------------------------------------------------------------------
//Initial Configurations
//------------------------------------------------------------------------
function opts() {
      opts.GetPlugins = function ()
      {
      return app.ReadFile("plugins.txt")
      }
      
      opts.LoadPlugins = function(list, debug) {
         opts.pluginlist  = list.split(",")
         for (var i in opts.pluginlist) {
            app.LoadPlugin(opts.pluginlist[i]);
            if(debug){app.Debug(opts.pluginlist[i].toString())}
         }
         return;
      }
      
        opts.GetScripts = function (lstDel)
      {   
        opts.scriptlist = app.ListFolder(app.GetAppPath(), ".js")
        for (var i in lstDel)
        {
           opts.removeitems(opts.scriptlist,lstDel[i]);
        }
          
          return opts.scriptlist
      }
           
      opts.LoadScripts = function (remove, toFront, debug) {
         opts.scriptlist  = opts.GetScripts(remove);
         //move to front (to avoid some sort of syncronization error that occurs)
         if (toFront.length > 0)
         {
         var arr = toFront.split(",");
          for (var i in arr){
          
                opts.scriptlist.unshift(arr[i]);
         }
         }
  
          for (var i in opts.scriptlist) {
            app.Script(opts.scriptlist[i]);
            if(debug){app.Debug(opts.scriptlist[i].toString())}
         }
         return;
      }
 
      
   opts.removeitems = function (arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

      opts.config = function() {
         _AddPermissions("Share,Storage,Accounts,Network");
         //app.SetOrientation( "portrait" );
         app.EnableBackKey(false);
         app.DisableKeys('VOLUME_DOWN,VOLUME_UP');
     
     //Scripts and Plugins
      opts.LoadPlugins(opts.GetPlugins(),true);
      uix = app.CreateUIExtras();
      web = app.CreateWebView(1, 0.955, "IgnoreErrors,AllowZoom");
      webd = app.CreateDialog("", "NoDim,NoTitle");
      layWeb = app.CreateLayout("frame");
      layWeb.SetSize(1, 1);
      layWeb.SetBackColor("#CC2c2b2a");
      ll = app.CreateLayout("linear", "VTop");
      ll.AddChild(web);
      layWeb.AddChild(ll);
      layNav = app.CreateLayout("linear", "Horizontal,HCenter,FillXY");
      layNav.SetSize(1, .045);
      btnClose = app.CreateButton("[fa-window-close]", (1 / 6), -1, "GrayButton,FontAwesome");
      btnBack = app.CreateButton("[fa-arrow-left]", (1 / 6), -1, "GrayButton,FontAwesome");
      btnForward = app.CreateButton("[fa-arrow-right]", (1 / 6), -1, "GrayButton,FontAwesome");
      btnReload = app.CreateButton("[fa-refresh]", (1 / 6), -1, "GrayButton,FontAwesome");
      btnRemote = app.CreateButton("[fa-external-link]", (1 / 6), -1, "GrayButton,FontAwesome");
      btnPrint = app.CreateButton("[fa-print]", (1 / 6), -1, "GrayButton,FontAwesome");
      
      btnBack.SetOnTouch(function() {
         web.Back();
      });
      btnForward.SetOnTouch(function() {
         web.Forward();
      });
      btnReload.SetOnTouch(function() {
         web.Reload();
      });
      btnClose.SetOnTouch(function() {
         webd.Hide();
      });
      btnRemote.SetOnTouch(function() {
         /*
               in progress ,open by filetype
               var cf =     app.LoadText("currentFile", null, "currentFile.txt")
               var cext = cf.substring(cf.lastIndexOf(".")+1,cf.length)
               switch (cext)
               {
               case "md":
               break;
               case "html":
               break;
   
               break;
   
               }
               app.ShowPopup( "cf\: "+cf+"\ncext\: "+cext );
                app.OpenFile(cf,cext)
                */
         var cf = app.LoadText("currentFile", null, "currentFile.txt")
         var cext = cf.substring(cf.lastIndexOf(".") + 1, cf.length)
         if (web.GetUrl() != "about:blank") {
            if (cext.indexOf("htm") > -1) {
               app.OpenUrl(web.GetUrl())
            }
         } else {
            app.OpenFile(app.LoadText("currentFile", web.GetUrl(), "currentFile.txt"))
         }
      });
      btnPrint.SetOnTouch(function() {
         web.Print()
      });
      layNav.AddChild(btnClose);
      layNav.AddChild(btnBack);
      layNav.AddChild(btnForward);
      layNav.AddChild(btnReload);
      layNav.AddChild(btnRemote);
      layNav.AddChild(btnPrint);
      ll.AddChild(layNav);
      layWeb.AddChild(ll);
      webd.AddLayout(layWeb);
      opts.dlgWs = app.CreateDialog("Set Workspace Path", "");
      opts.pathDat = app.LoadText("pathDat", "/sdcard/Protoeditor", "pathDat.txt")
      opts.txtWs = app.CreateTextEdit(opts.pathDat)
      opts.btnWsY = app.CreateButton("Ok");
      opts.btnWsN = app.CreateButton("Cancel");
      opts.btnWsY.SetOnTouch(function() {
         app.ShowProgress("Copying Files to\: \n " + opts.txtWs.GetText());
         app.SaveText("pathDat", opts.txtWs.GetText(), "pathDat.txt");
         app.ExtractAssets("Misc/databackup", (opts.txtWs.GetText() + "/").replace("\/\/", "\/"), true);
         app.ShowProgress((opts.txtWs.GetText() + "/").replace("\/\/", "\/"))
         app.ExtractAssets("Misc/build", (opts.txtWs.GetText() + "/build").replace("\/\/", "\/"), true);
         app.ShowProgress((opts.txtWs.GetText() + "/build").replace("\/\/", "\/"))
         app.SaveBoolean("assetsExt", true, "assetsExt.txt");
         app.HideProgress();
         opts.dlgWs.Dismiss();
      });
      opts.btnWsN.SetOnTouch(function() {
         opts.dlgWs.Dismiss();
      });
      opts.layWs = app.CreateLayout("Linear", "VTop");
      opts.layWs.AddChild(opts.txtWs);
      opts.layWs.AddChild(opts.btnWsY);
      opts.layWs.AddChild(opts.btnWsN);
      opts.dlgWs.AddLayout(opts.layWs);
      return;
   } //end opts.config
opts.extract = function() {
      //app.SaveText("pathDat", pathDat, "pathDat.txt");
      //app.SaveBoolean("assetsExt", false, "assetsExt.txt");
      //	app.HideProgress();
      if (!app.LoadBoolean("assetsExt", false, "assetsExt.txt")) {
         opts.dlgWs.Show();
         app.SaveBoolean("assetsExt", true, "assetsExt.txt");
      }
      new browser();
      pathDocs = app.CreateText("");
      pathData = app.CreateText("");
      pathDocs.SetText(app.LoadText("wspaceSave", null, "wspaceSave.txt"))
      pathData.SetText(app.LoadText("pathDat", null, "pathDat.txt"))
      return;
   } //end opts.extract
} //end opts
function hwShorts() {
   app.SetOnKey(function(action, name) {
      /*debug*/ //app.ShowPopup( action + " " + name );
      if (name == "VOLUME_DOWN" && action == "Down") {
         edtTxt.SetCursorPos(edtTxt.GetCursorPos() - 1)
      }
      if (name == "VOLUME_UP" && action == "Down") {
         edtTxt.SetCursorPos(edtTxt.GetCursorPos() + 1)
      }
   });
}
//var keybar = new extraKeyBar
var isEdtA = false;
var isEdtB = false;
app.SaveBoolean("isDebug", false);
if (!app.LoadBoolean("isLocked", false, "isLocked.txt")) {
   app.SetOrientation(app.GetOrientation());
   app.ShowPopup("🔐");
} else {
   app.ShowPopup("🔓");
}