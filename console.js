var isGo = true;
//Called when application is started.
function OnStart()
{   
frame = app.CreatelayConsoleout("frame")
frame.SetSize(1,1)

	//Create a layConsoleout with objects vertically centered.
	layConsole = app.CreatelayConsoleout( "linear", "VCenter,FillXY" );
	layConsole.SetBackColor( "#2c2b2a" );
	
	//Create a scroller for log window.
    scroll = app.CreateScroller( 0.945, 0.81);
    scroll.SetBackColor( "#392e34" );
    layConsole.AddChild( scroll );
  
  layConsoleEntry = app.CreatelayConsoleout("linear","Left,FillXY");
  txtCarrot = app.CreateText("[fa-chevron-right]", 1,-1,"FontAwesome");
  scrCarrot = app.CreateScroller(1,-1)
  scrCarrot.AddChild(txtCarrot)
  layConsoleEntry.AddChild(scrCarrot);
	//Create text control for logging (max 500 lines).
	txt = app.CreateText( "su", .81,0.18, "Log,Monospace,NoSpell" );
txt.SetTouchable(true)
		txt.SetBackColor( "#392e34" );
	txt.SetLog( 500 );
	scroll.AddChild( txt );
	
	//Create an text edit box for entering commands.
    edt = app.CreateTextEdit( "netstat", 0.9, 0.18, "SingleLine" );
  //  edt.SetMargins( 0,8,0,0, "dip" );
    edt.SetBackColor( "#2c2b2a" );
    edt.SetOnEnter( edt_OnEnter );
   // edt.SetHint( ">" );
    layConsoleEntry.AddChild( edt );
    layConsole.AddChild(layConsoleEntry);
	//Add layConsoleout to app.	
 frame.AddChild(layConsole)
return frame
	 
	//Create system process.
	sys = app.CreateSysProc( "sh" );
	sys.SetOnInput( sys_OnInput );
	sys.SetOnError( sys_OnError );
	
    //Detect keyboard showing.
    app.SetOnShowKeyboard( app_OnShowKeyBoard );
    
    //Disable debug so logs don't fill up.
	app.SetDebugEnabled( false );
	
	//Run the netstat command.
	setTimeout( edt_OnEnter, 1000 );
}

//Called when we get data from the input stream.
function sys_OnInput( data )
{
    //Write data to log.
    
 
 

    

    if ( data.indexOf("curdir") > -1){
     txtCarrot.SetText("[fa-chevron-right]"+data.replace("curdir","")+ "\n")
    isGo = false;
    }
    else { 
    isGo =true;
    }
    
    if (isGo != false){
   txt.Log( data );
 
   }
    //Scroll to bottom if lines added.
    setTimeout( Scroll, 100 );
}

//Called when we get errors from the input stream.
function sys_OnError( data )
{
    //Write data to log.
    txt.Log( data, "Red" );
 
    //Scroll to bottom if lines added.
    setTimeout( Scroll, 100 );
}

//Handle enter key.
function edt_OnEnter()
{

    var cmd = edt.GetText();
if (cmd.indexOf("cd" > -1)){
     sys.Out("echo curdir && pwd" + "\n");   
     }

edt.SetText("");
    //Add command to log window.
    txt.Log( cmd+"\n", "Green" );
    
    //Send command to output stream.
    sys.Out( cmd+"\n" );
  

  
}

//Handle soft-keyboard show and hide.
function app_OnShowKeyBoard( shown )
{
    //Get fractional height of keyboard.
    var kbh = app.GetKeyboardHeight() / app.GetDisplayConsoleHeight();
    
    if( shown ) scroll.SetSize( 0.95, 0.9-kbh );
    else scroll.SetSize( 0.95, 0.9 );
    Scroll();
}

//Scroll to bottom of log window.
function Scroll()
{
    scroll.ScrollTo( 0, 999 );
}