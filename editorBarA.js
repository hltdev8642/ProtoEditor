//------------------------------------------------------------------------
//editorBarA
//------------------------------------------------------------------------
function editorBarA() {
      this.init = function(barH) {
         this.edtBarBackColor = "#2c2b2a";
         this.syntaxList = "Syntax:" + "," + "html,php,javascript,python,java,text,markdown,c,ruby,arduino,source,css,image,audio,video,pdf"
         this.globalTextSize = 14.4
         editorBarA.h = barH || -1
         editorBarA.edtBar = app.CreateLayout("Linear", "Horizontal");
         editorBarA.edtBar.SetSize(1, editorBarA.h);
         editorBarA.edtBar.SetBackColor(this.edtBarBackColor);
         editorBarA.edtBar.SetMargins(0, 0, 0, 0);
         var btnsedtBarA = ["[fa-bars]", "[fa-save]", "[fa-folder-open]", "[fa-play]", "[fa-bug]", "[fa-chrome]","[fa-lock]"];
         var wBarA = parseFloat((1 / (btnsedtBarA.length + 3)))
         for (var i = 0; i < btnsedtBarA.length; i++) {
            btnsA = app.CreateList(btnsedtBarA[i], wBarA, -1, "FontAwesome,monospace");
            btnsA.SetColumnWidths(0, 1, 0);
            btnsA.SetTextSize(this.globalTextSize);
            btnsA.icon = btnsedtBarA[i];
            btnsA.SetOnTouch(keys_OnTouch)
            btnsA.SetOnLongTouch(keys_OnLTouch);
            editorBarA.edtBar.AddChild(btnsA);
         }
         spnSyntax = app.CreateSpinner(this.syntaxList)
         spnSyntax.SetTextSize(this.globalTextSize);
         spnSyntax.SetOnChange(function(item) {
            switch (item) {
               case "markdown":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".md");
                  break;
               case "text":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".txt");
                  break;
               case "html":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".html");
                  break;
               case "css":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".css");
                  break;
               case "python":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".py");
                  break;
               case "php":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".php");
                  break;
               case "javascript":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".js");
                  break;
               case "java":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".java");
                  break;
               case "python":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".py");
                  break;
               case "c":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".c");
                  break;
               case "ruby":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".rb");
                  break;
               case "arduino":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".c");
                  break;
               case "ruby":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".rb");
                  break;
               case "source":
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".rb");
                  break;
               case "image":
                  app.SaveBoolean("isProtected", true, "isProtected.txt");
                  app.ShowPopup("Protection Enabled");
                  edtTxt.SetLanguage("Syntax:");
                  break;
               case "video":
                  app.SaveBoolean("isProtected", true, "isProtected.txt");
                  app.ShowPopup("Protection Enabled");
                  edtTxt.SetLanguage("Syntax:");
                  break;
               case "audio":
                  app.SaveBoolean("isProtected", true, "isProtected.txt");
                  app.ShowPopup("Protection Enabled");
                  edtTxt.SetLanguage("Syntax:");
                  break;
               case "pdf":
                  app.SaveBoolean("isProtected", true, "isProtected.txt");
                  app.ShowPopup("Protection Enabled");
                  edtTxt.SetLanguage("Syntax:");
                  break;
               default:
                  app.SaveBoolean("isProtected", false, "isProtected.txt");
                  edtTxt.SetLanguage(".js");
                  break;
            } //end switch
         });
         editorBarA.edtBar.AddChild(spnSyntax);
         return editorBarA.edtBar;
      }
   } //end.editorBarA