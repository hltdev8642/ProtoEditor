//------------------------------------------------------------------------
//drawers
//------------------------------------------------------------------------
function drawer() {
      //setTheme()
      drawer.buildLeft = function(url) {
            drawer.url = url || "http://www.google.com";
            drawer.tabs = app.CreateTabs(" File Browser, PDF Viewer", 1, 1);
            drawer.layDrawer = drawer.tabs.GetLayout(" File Browser")
            drawer.layPDF = drawer.tabs.GetLayout(" PDF Viewer")
            drawer.webpdf = app.CreateWebView(1, 1, "IgnoreErrors");
            drawer.FCBar2 = app.CreateLayout("Linear", "Horizontal");
            drawer.layFileC = app.CreateLayout("Linear", "Horizontal");
            drawer.txtFP = app.CreateButton("Current File: ");
            drawer.txtFP.SetMargins(null, .036, null, null);
            drawer.txtFP.SetTextColor("#783873");
            drawer.barFileCurr = app.CreateTextEdit(this.path || "/", -1, .116, "MultiLine,ReadOnly,NoKeyboard,NoSpell,,AutoSize,AutoSelect");
            drawer.barFileCurr.SetTextColor("#793939");
            drawer.barFileCurr.SetBackColor("#00000000");
            drawer.barFileCurr.SetPadding(0, .0491184, .009, 0);
            drawer.barFileCurr.SetMargins(0, 0, 0, 0)
            drawer.barFileCurr.SetCursorColor("#CC5c2b2a");
            drawer.txtFP.SetOnTouch(function() {
               app.OpenFile(drawer.GetPath(), drawer.GetPath().substring(-1, drawer.GetPath().lastIndexOf(".")));
            });
            drawer.layBarP = app.CreateLayout("Linear", "Horizontal,touchthrough");
            drawer.layBarP.SetGravity("VCenter");
            drawer.txtFileCreate = app.CreateText("", 1);
            drawer.layfp = app.CreateLayout("Linear", "Vertical");
            drawer.btnLabelRecents = app.CreateText("Long Press File to Append Data to Clipboard", -1, null, "fontawesome");
            drawer.btnClearR = app.CreateButton("Clear Recents");
            drawer.btnClearR.SetOnTouch(function() {
               app.SaveText("listRecents", " ", "listRecents.txt");
               listRecents.SetList(app.LoadText("listRecents", null, "listRecents.txt"));
            });
            listRecents = app.CreateList(app.LoadText("listRecents", null, "listRecents.txt"), 1, .45, "");
            recentsBackDrop = app.CreateLayout("Linear", "VTop,fillxy");
            recentsBackDrop.AddChild(listRecents);
            recentsBackDrop.SetBackColor("#CC2c2b2a");
            recentsBackDrop.SetBackAlpha(.34);
            recentsBackDrop.SetSize(.981, .63);
            listRecents.SetTextSize(11.7);
            drawer.btnCloseR = app.CreateButton("[fa-close] Close", 1, null, "fontawesome");
            drawer.btnCloseR.SetOnTouch(function() {
               dlgRecents.Dismiss();
            });
            dlgRecents = app.CreateDialog("Recent Files", "");
            dlgRecents.SetBackColor( "#2c2b2a" );
            dlgRecents.AddLayout(drawer.layfp);
            new picker();
            picker.buildUI();
            drawer.foldp = picker.init();
            drawer.layFiles = app.CreateLayout("Linear", "Vertical");
            drawer.layFiles.SetSize(1, .63);
            drawer.btnLevelUp = app.CreateButton("[fa-level-up]", 1, 0.09, "fontawesome,fillxy");
            drawer.btnLevelUp.SetOnTouch(function() {
               window.OnBack()
            });
            drawer.txtSearchBarFC = app.CreateTextEdit("");
            drawer.layDrawer.AddChild(drawer.FCBar2);
            drawer.layFileC.AddChild(drawer.txtFP);
            drawer.layBarP.AddChild(drawer.barFileCurr);
            drawer.layFileC.AddChild(drawer.layBarP);
            drawer.layDrawer.AddChild(drawer.layFileC);
            drawer.layDrawer.AddChild(drawer.txtFileCreate);
            drawer.layfp.AddChild(drawer.btnLabelRecents);
            drawer.layfp.AddChild(drawer.btnClearR);
            drawer.layfp.AddChild(recentsBackDrop);
            drawer.layfp.AddChild(drawer.btnCloseR);
            drawer.layFiles.AddChild(drawer.foldp);
            drawer.layDrawer.AddChild(drawer.layFiles);
            drawer.layDrawer.AddChild(drawer.btnLevelUp);
            drawer.layPDF.AddChild(drawer.webpdf);
            drawer.tabLay = app.CreateLayout("linear", "VTop,Left");
            drawer.tabLay.AddChild(drawer.tabs);
            drawer.scrDLeft = app.CreateScroller(-1, -1);
            drawer.scrDLeft.AddChild(drawer.tabLay);
            app.AddDrawer(drawer.scrDLeft, "left", 1, .009);
         } //end drawer.tabs
      drawer.SetPath = function(path) {
            this.path = path || drawer.path;
            drawer.barFileCurr.SetText(this.path);
         } // end drawer.SetPath
      drawer.GetPath = function(path) {
            return this.path
         } // end drawer.GetPath
      drawer.GetUrlLeft = function() {
         return drawer.webpdf.GetUrl();
      }
      drawer.buildRight = function() {
         cbar = new clipBar();
         cbar.init();
         scrDRight = app.CreateScroller(-1, -1);
         scrDRight.AddChild(cbar.init())
         app.AddDrawer(scrDRight, "right", .216, 0.009);
      }
   } //end.drawer