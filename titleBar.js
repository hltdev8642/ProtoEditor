//------------------------------------------------------------------------
//titleBar.js 
//------------------------------------------------------------------------
function titleBar() {
      //titlebar.constructors
      titleBar.CreateTitleBar = function(titleText, textTitleSize) {
            titleBar.titleText = titleText || "ProtoEditor"
            titleBar.titleTextSize = textTitleSize || 12
            titleBar.layMain = app.CreateLayout("Linear", "Vertical,Left");
            titleBar.layMain.SetGravity("VCenter,HCenter");
            titleBar.txtTitle = app.CreateText(titleBar.titleText);
            titleBar.txtTitle.SetTextSize(titleBar.titleTextSize);
            titleBar.layA = app.CreateLayout("Linear", "Horizontal");
            titleBar.layB = app.CreateLayout("Linear", "Horizontal");
            titleBar.layTitle = app.CreateLayout("linear", "horizontal");
            titleBar.layTitle.SetGravity("Left");
            titleBar.scrTitle = app.CreateScroller(null, null, "NoScrollBars");
            titleBar.txtPath = app.CreateText(titleBar.textPath || "/");
            titleBar.layA.AddChild(titleBar.txtTitle);
            titleBar.layB.AddChild(titleBar.txtPath);
            titleBar.layTitle.AddChild(titleBar.layA);
            titleBar.scrTitle.AddChild(titleBar.layB);
            titleBar.layTitle.AddChild(titleBar.scrTitle);
            titleBar.layMain.AddChild(titleBar.layTitle);
            return titleBar.layMain
         } //titleBar.CreateTitleBar
      titleBar.SetStyle = function(height, alpha, colorBGTitle, colorTxtTitle, colorTxtPath, colorBGPath, textPathSize) {
            titleBar.height = height || -1
            titleBar.alpha = alpha || titleBar.alpha || 1.0
            titleBar.colorBGTitle = colorBGTitle
            titleBar.colorTxtTitle = colorTxtTitle
            titleBar.colorTxtPath = colorTxtPath
            titleBar.colorBGPath = colorBGPath
            titleBar.textPathSize = textPathSize || 12
            titleBar.layTitle.SetBackColor(titleBar.colorBGTitle);
            titleBar.txtPath.SetTextSize(titleBar.textPathSize)
            titleBar.txtTitle.SetBackColor(titleBar.colorBGTitle);
            titleBar.txtTitle.SetTextColor(titleBar.colorTxtTitle);
            titleBar.txtTitle.SetBackAlpha(titleBar.alpha);
            titleBar.layMain.SetBackColor(titleBar.colorBGMain);
            titleBar.txtPath.SetTextColor(titleBar.colorTxtPath);
            titleBar.txtPath.SetBackAlpha(titleBar.alpha);
            return;
         } //titleBar.SetStyle
      titleBar.SetPath = function(path) {
            this.path = path || titleBar.path
            titleBar.txtPath.SetText(this.path);
            return;
         } //end titleBar.SetPath
      titleBar.GetPath = function(path) {
            return titleBar.txtPath.GetText();
         } //end titleBar.GetPath
   } //end titleBar()