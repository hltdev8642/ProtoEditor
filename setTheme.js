//------------------------------------------------------------------------
//setTheme.js 
//------------------------------------------------------------------------
function setTheme() {
      // app.SetStatusBarColor( "#783873" );
      // app.SetNavBarColor( "#783873" );
      theme = app.CreateTheme("Dark");
      theme.SetDimBehind(false);
      theme.SetButtonStyle("#353535", "#161616", 2, "#2c2b2a", 0, 1, "#978873");
      theme.AdjustColor(35, 0, -10);
      theme.SetBtnTextColor("white");
      theme.SetButtonOptions("custom");
      theme.SetBackColor("#2c2b2a");
      theme.SetCheckBoxOptions("dark");
      theme.SetTextEditOptions("NoDim");
      theme.SetDialogColor("#2c2b2a");
      theme.SetDialogBtnColor("#2c2b2a");
      theme.SetDialogBtnTxtColor("white");
      app.SetTheme(theme);
   } //end.setTheme