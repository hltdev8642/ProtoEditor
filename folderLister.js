//------------------------------------------------------------------------
//folderLister.js
//------------------------------------------------------------------------
/*
 * Credit to John of the DS forums for all code within folderLister.js
 * Also a big thank you to him, as these methods are not only very
 * useful, but also crucial to the functionality of this app
 * Link:[https://grou//ps.google.com/d/msg/droidscriptpremium/J95-ER0xZ7I/7ZjoElG3AAAJ]
 */
function FolderLister(path) {
      this.SetPath(path);
   } //end.FolderLister
FolderLister.prototype.SetPath = function(path) {
      if (typeof path === "string") {
         if (path.charAt(0) === "/") {
            this.path = path;
         } else if (path === "..") {
            this.path = this.path.split("/").slice(0, -1).join("/") || "/sdcard";
         } else {
            this.path = this.path + "/" + path;
         }
         edt.SetText(this.path);
      }
      app.EnableBackKey(this.path === "/sdcard");
   } //end.FolderLister.SetPath
FolderLister.prototype.MakeList = function(options, textSize) {
      var FolderLister = this;
      this.options = options || "";
      textSize = textSize || 12;
      /*folders*/ //this.list = app.CreateList(app.ListFolder(this.path,null, null, this.options + ",folders"));
      /*all*/
      this.list = app.CreateList(app.ListFolder(this.path, null, null, this.options));
      this.list.SetTextSize(textSize);
      this.list.SetOnTouch(function(path) {
         /*Checks if folder or file*/
         var fileToOpen = edt.GetText() + "/" + path
         app.SaveText("wsDir", fileToOpen.substring(0, fileToOpen.lastIndexOf("/")), "wsDir.txt")
            //if a folder, then update folder list
         if (path.indexOf(".") <= -1) {
            FolderLister.SetPath(path);
            FolderLister.update();
         }
         // if a file, send path to file opening utility 
         else {
         if (app.FolderExists(fileToOpen))
         {
            FolderLister.SetPath(path);
            FolderLister.update();
         }
         
         else
         {
            app.CloseDrawer("left");
            onFileChoose(fileToOpen)
            }
         }
      });
      window.OnBack = function() {
         FolderLister.SetPath("..")
         FolderLister.update();
      };
      window.update = function() {
         FolderLister.update();
      };
      app.EnableBackKey(false);
      return this.list;
   } //end.FolderLister.MakeList
FolderLister.prototype.update = function() {
      /*folders*/ //this.list.SetList(app.ListFolder(this.path, null, null, this.options + ",folders"));
      /*all*/
      this.list.SetList(app.ListFolder(this.path, null, null, this.options));
   } //end.FolderLister.update
FolderLister.prototype.uplvl = function() {
      /*folders*/ //this.list.SetList(app.ListFolder(this.path, null, null, this.options + ",folders"));
      /*all*/
      this.list.SetList(app.ListFolder(this.path, null, null, this.options));
   } //end.FolderLister.uplvl
FolderLister.prototype.GetPath = function() {
      return this.path
   } //end.FolderLister.GetPath
//------------------------------------------------------------------------
//Touch.js 
//------------------------------------------------------------------------
function keys_OnTouch() {
      switch (this.icon) {
         case "[fa-undo]":
            edtTxt.Undo();
            break;
         case "[fa-close]":
            edtTxt.SetText(" ");
            break;
         case "[fa-copy]":
            edtTxt.Copy()
            break;
         case "[fa-paste]":
            edtTxt.Paste();
            break;
         case "[fa-arrows-alt]":
            edtTxt.SelectAll();
            break;
         case "[fa-play]":
            var syntax = spnSyntax.GetText();
            app.SaveBoolean("isDebug", false);
            run(syntax);
            break;
         case "[fa-lock]":
      
               if (!app.LoadBoolean( "isLocked",false, "isLocked.txt")){
                  app.SaveBoolean( "isLocked" , true, "isLocked.txt" );
                  app.SaveBoolean("isConfig", false, "isConfig.txt");
                  app.SetOrientation( app.GetOrientation());
                  app.ShowPopup( "🔐" );
                  }
               else {
               app.SaveBoolean( "isLocked" , false, "isLocked.txt" );
               app.SaveBoolean("isConfig", true, "isConfig.txt");
               app.SetOrientation( null );
               app.ShowPopup( "🔓" );
               }
               
            break;
         case "[fa-repeat]":
            edtTxt.Redo();
            break;
         case "[fa-chrome]":
            browser.build()
            browser.laywebb.Animate("SlideFromTop");
            break;
         case "[fa-search]":
            edtTxt.Search(searchF.GetText());
            break;
         case "[fa-times-circle]":
            edtTxt.SetText("");
            break;
         case "[fa-exchange]":
            edtTxt.Search(searchF.GetText());
            edtTxt.Replace(searchR.GetText());
            break;
         case "[fa-cut]":
            edtTxt.Cut()
            break;
         case "[fa-save]":
            if (!(app.LoadBoolean("isProtected", false, "isProtected.txt"))) {
               dlgSave = app.CreateDialog("are you sure?", "NoDim");
               layDlgSave = app.CreateLayout("Linear", "VCenter");
               //	layDlgSave.SetSize(.45, .27);
               optSaveA = app.CreateButton(" [fa-check] \n Save ", -1, null, "FontAwesome");
               optSaveA.SetOnTouch(function() {
                  var file = app.LoadText("currentFile", null, "currentFile.txt");
                  app.WriteFile(file, app.ReadFile(file));
                  app.WriteFile(file, edtTxt.GetText());
                  dlgSave.Dismiss();
                  app.ShowPopup("Saved:" + " \n" + file);
                  return;
               });
               optSaveB = app.CreateButton(" [fa-window-close] \n Cancel ", -1, null, "FontAwesome");
               optSaveB.SetOnTouch(function() {
                  dlgSave.Dismiss();
               });
               optSaveC = app.CreateButton(" [fa-file] \n Save As ", -1, null, "FontAwesome");
               optSaveC.SetOnTouch(function() {
                  saveaspath = app.CreateTextEdit("/sdcard/");
                  laysaveas = app.CreateLayout("Linear", "Horizontal");
                  laysaveas.AddChild(saveaspath);
                  dlgsaveas = app.CreateDialog("", "NoDim");
                  btnsaveasdlg = app.CreateButton("save to path below");
                  laysaveas.AddChild(btnsaveasdlg);
                  dlgsaveas.AddLayout(laysaveas);
                  btnsaveasdlg.SetOnTouch(function() {
                     var pathSaveAs = saveaspath.GetText();
                     app.ShowPopup("(will) Save to path :" + saveaspath.GetText());
                  });
                  dlgsaveas.Show();
               });
               layDlgSave.AddChild(optSaveA);
               layDlgSave.AddChild(optSaveB);
               layDlgSave.AddChild(optSaveC);
               dlgSave.AddLayout(layDlgSave);
               dlgSave.Show();
            }
            break;
         case "[fa-share-alt]":
            app.SendText(edtTxt.GetText());
            break;
         case "[fa-bars]":
            app.OpenDrawer("left");
            break;
         case "[fa-folder-open]":
            var oldFile = app.LoadText("currentFile", null, "currentFile.txt");
            app.SaveText("lastFile", oldFile, "lastFile.txt");
            openFile()
            app.CloseDrawer("left");
            break;
         case "[fa-bug]":
            var syntax = spnSyntax.GetText();
            app.SaveBoolean("isDebug", true);
            run(syntax);
            break;
      } //end.switch
   } //end.keys_OnTouch
function keys_OnLTouch() {
      switch (this.icon) {
         case "[fa-save]":
            if (!(app.LoadBoolean("isProtected", false, "isProtected.txt"))) {
               var file = app.LoadText("currentFile", null, "currentFile.txt");
               app.WriteFile(file, app.ReadFile(file));
               app.WriteFile(file, edtTxt.GetText());
               app.ShowPopup("Saved:" + " \n" + file);
            }
            break;
         case "[fa-play]":
            app.SaveBoolean("isDebug", false);
            run("quicksand")
            break;
         case "[fa-exchange]":
            edtTxt.ReplaceAll(searchF.GetText(), searchR.GetText());
            break
            return;
      } //end.switch
   } //end.keys_OnLTouch
//------------------------------------------------------------------------
//picker.js 
//------------------------------------------------------------------------
function picker() {
      picker.buildUI = function() {
            picker.layd = app.CreateLayout("linear");
            picker.layMake = app.CreateLayout("Linear", "Horizontal");
            picker.layd.AddChild(picker.layMake);
            picker.btnRecents = app.CreateButton("[fa-cubes] Recents", 0.45, null, "fontawesome");
            picker.btnRecents.SetOnTouch(function() {
               var recents = app.LoadText("listRecents", null, "listRecents.txt")
               listRecents.SetOnTouch(function(title) {
                  app.CloseDrawer("left");
                  dlgRecents.Dismiss()
                  app.SaveText("currentFile", title, "currentFile.txt");
                  app.SaveText("oldListItemFile", title, "oldListItemFile.txt")
                  onFileChoose(title)
                  listRecents.RemoveItem(app.LoadText("oldListItemFile", null, "oldListItemFile.txt"));
               });
               listRecents.SetOnLongTouch(function(title) {
                  var clipBC = app.ReadFile(title);
                  app.SetClipboardText(clipBC);
                  app.ShowPopup("Copied: \n " + clipBC)
               });
               dlgRecents.Show();
            });
            picker.layMake.AddChild(picker.btnRecents);
            picker.btnOpen = app.CreateButton("[fa-folder-open] Open File", 0.45, null, "fontawesome");
            picker.btnOpen.SetOnTouch(function() {
               var oldFile = app.LoadText("currentFile", null, "currentFile.txt");
               app.SaveText("lastFile", oldFile, "lastFile.txt");
               openFile()
               app.CloseDrawer("left");
            });
            picker.layMake.AddChild(picker.btnOpen);
            picker.layFCBar = app.CreateLayout("Linear", "Horizontal");
            picker.layd.AddChild(picker.layFCBar);
            picker.btnFileNewFC = app.CreateButton("[fa-file] Create File", 0.333, null, "fontawesome");
            picker.btnFileNewFC.SetOnTouch(function() {
               if (txtFileNameFC.GetText().length != 0) {
                  var newFP = edt.GetText() + "/" + txtFileNameFC.GetText() + "." + txtFileType.GetText()
                  var extension = newFP.substring(newFP.lastIndexOf("."));
                  app.WriteFile(newFP, edtTxt.GetText());
                  app.ShowPopup("Created File: " + newFP);
                  /*refreshes file browser list @ current path*/
                  window.update()
                  app.SaveText("currentFile", newFP, "currentFile.txt");
                  /*debug*/ //app.ShowPopup("new cf is " + app.LoadText("currentFile", null, "currentFile.txt"))
                  drawer.SetPath(newFP);
                  titleBar.SetPath(newFP);
                  listRecents.AddItem(newFP);
                  app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
                     //var listr = app.LoadText("listRecents", null, "listRecents.txt");
                     //listRecents.SetList(listr, ",")
                  switch (extension) {
                     //.js,.java,.php,.c,.cpp,.cs,.rb,.m,.py,.txt
                     case ".md":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("markdown");
                        edtTxt.SetLanguage(".m");
                        break;
                     case ".txt":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("text");
                        edtTxt.SetLanguage(".txt");
                        break;
                     case ".text":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("text");
                        edtTxt.SetLanguage(".text");
                        break;
                     case ".html":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("html");
                        edtTxt.SetLanguage(".html");
                        break;
                     case ".css":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("css");
                        edtTxt.SetLanguage(".html");
                        break;
                     case ".php":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("php");
                        edtTxt.SetLanguage(".js");
                        break;
                     case ".js":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("javascript");
                        edtTxt.SetLanguage(".js");
                        break;
                     case ".java":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("java");
                        edtTxt.SetLanguage(".java");
                        break;
                     case ".class":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("java");
                        edtTxt.SetLanguage(".java");
                        break;
                     case ".py":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("python");
                        edtTxt.SetLanguage(".py");
                        break;
                     case ".c":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("c");
                        edtTxt.SetLanguage(".c");
                        break;
                     case ".rb":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("ruby");
                        edtTxt.SetLanguage(".rb");
                        break;
                     case ".ino":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("arduino");
                        edtTxt.SetLanguage(".cpp");
                        break;
                     case ".pde":
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("arduino");
                        edtTxt.SetLanguage(".cpp");
                        break;
                     default:
                        app.SaveBoolean("isProtected", false, "isProtected.txt");
                        spnSyntax.SetText("Syntax");
                        edtTxt.SetLanguage(".txt");
                        break;
                  }
               }
            });
            picker.layFCBar.AddChild(picker.btnFileNewFC);
            picker.btnTmplNewFC = app.CreateButton("[fa-sitemap] Templates", 0.333, null, "fontawesome");
            picker.btnTmplNewFC.SetOnTouch(function() {
               app.ShowPopup("tbd");
            });
            picker.layFCBar.AddChild(picker.btnTmplNewFC);
            picker.btnFldrNewFC = app.CreateButton("[fa-folder] Create Folder", 0.333, null, "fontawesome");
            picker.btnFldrNewFC.SetOnTouch(function() {
               if (txtFileNameFC.GetText().length != 0) {
                  app.MakeFolder(edt.GetText() + "/" + txtFileNameFC.GetText());
                  alert("Created Folder:" + edt.GetText() + "/" + txtFileNameFC.GetText());
                  /*refreshes file browser list @ current path*/
                  window.update()
               }
            });
            picker.layFCBar.AddChild(picker.btnFldrNewFC);
            barnf = app.CreateLayout("Linear", "Horizontal");
            picker.layd.AddChild(barnf);
            labelDot = app.CreateText(".");
            txtFileNameFC = app.CreateTextEdit("", 0.45, null, "singleline,nospell,autosize,autoselect");
            txtFileNameFC.SetHint("File/Folder");
            barnf.AddChild(txtFileNameFC);
            barnf.AddChild(labelDot);
            txtFileType = app.CreateTextEdit("", .225, -1, "singleline,nospell,autosize,autoselect");
            txtFileType.SetHint("ext");
            spnFileType = app.CreateSpinner("txt,js,html,css,json,dat,temp,py,md,java,php,ino,pde,c,h,class,rb,source", 0.225, -1);
            spnFileType.SetOnChange(function(item) {
               //to strip off ".ext" from filename
               //var tftCurrent = txtFileType.GetText().substring(txtFileType.GetText().lastIndexOf(".")+1); 
               txtFileType.SetText(item);
            });
            barnf.AddChild(txtFileType);
            barnf.AddChild(spnFileType);
            edt = app.CreateText("");
            picker.layd.AddChild(edt);
            picker.lay = app.CreateLayout("linear");
            picker.lay.AddChild(picker.layd)
         } //end picker.BuildUI
      picker.init = function() {
            var pathWorkspace = app.LoadText("wsDir", null, "wsDir.txt");
            var fld = new FolderLister(pathWorkspace);
            var list = fld.MakeList("alphasort")
            list.SetOnLongTouch(function(title) {
               var pathDelete = edt.GetText() + "/" + title
               dlg = app.CreateYesNoDialog("Delete: " + title + " ?");
               dlg.SetOnTouch(function(item) {
                  if (item == "Yes") {
                     app.ShowPopup("Deleted: " + pathDelete);
                     app.DeleteFolder(pathDelete);
                     window.update()
                  }
                  if (item == "No") {
                     app.ShowPopup("no");
                  }
               });
               dlg.Show();
            });
            picker.lay.AddChild(list);
            return picker.lay
         } //end picker.init
   } //end.picker