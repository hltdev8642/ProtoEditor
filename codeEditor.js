//------------------------------------------------------------------------
//codeEditor
//------------------------------------------------------------------------
function codeEditor() {
      this.init = function(codeEditHeight) {
         codeEditor.h = codeEditHeight
         codeEditor.w = .90
         edtTxt = app.CreateCodeEdit("code editor", codeEditor.w, codeEditor.h);
         if (app.LoadBoolean("AutoOn", null, "AutoOn.txt")) {
            edtTxt.SetOnChange(refilter)
         } else {}
         edtTxt.SetColorScheme("Dark");
         edtTxt.SetBackColor("#783873");
         edtTxt.SetBackAlpha(.09)
         edtTxt.SetOnDoubleTap(function() {
            app.OpenDrawer("right");
         });
         /*enable for key shortcut commandoutput*/
         /*edtTxt.SetOnChange(function (title) {*/
         /*gets current key*/ //app.ShowPopup(edtTxt.GetText().substring(edtTxt.GetCursorPos(), edtTxt.GetCursorPos() -1))});
         return edtTxt;
      }
   } //end.codeEditor
function keybd(shown) {
      keybd.init = function() {
         keybd.width = app.GetScreenWidth()
         keybd.height = app.GetScreenHeight()
         keybd.sizeFactor = 0.00
         if (app.GetOrientation() == "Portrait") {
            keybd.sizeFactor = 0.056;
         }
         if (app.GetOrientation() == "Landscape") {
            keybd.sizeFactor = keybd.sizeFactor
         }
         keybd.sizeAll()
      }
      keybd.sizeAll = function() {
         keybd.widthFinal = keybd.width * codeEditor.w
         keybd.heightFinal = keybd.height * (codeEditor.h + keybd.sizeFactor)
         keybd.heightFinal -= app.GetKeyboardHeight()
         layM.codeEdit.SetSize(keybd.widthFinal, keybd.heightFinal, "px");
         //debug 
         /*
         app.ShowPopup( "width" + keybd.widthFinal );
         app.ShowPopup( "height" + keybd.heightFinal );	
         app.ShowPopup("factor" + keybd.sizeFactor)
         */
      }
   } //end.keybd