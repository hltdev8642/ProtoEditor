app.LoadScript('Misc/beautify_js.js');
//------------------------------------------------------------------------
//clipBar
//------------------------------------------------------------------------
function clipBar() {
      this.init = function() {
         layDrawerClipBarB = app.CreateLayout("linear", "Vertical");
         layDrawerClipBarA = app.CreateLayout("linear", "Vertical");
         layDrawerClipBarC = app.CreateLayout("Linear", "horizontal");
         divA = app.CreateList(",", null, .0045);
         divA.SetBackColor("#4c4b4a");
         divB = app.CreateList(",", null, .0045);
         divB.SetBackColor("#4c4b4a");
         divC = app.CreateList(",", null, .0045);
         divC.SetBackColor("#4c4b4a");
         divD = app.CreateList(",", null, .0045);
         divD.SetBackColor("#4c4b4a");
         divE = app.CreateList(",", null, .0045);
         divE.SetBackColor("#4c4b4a");
         divF = app.CreateList(",", null, .0045);
         divF.SetBackColor("#4c4b4a");
         divG = app.CreateList(",", null, .0045);
         divG.SetBackColor("#4c4b4a");
         divH = app.CreateList(",", null, .0045);
         divH.SetBackColor("#4c4b4a");
         divI = app.CreateList(",", null, .0045);
         divI.SetBackColor("#4c4b4a");
         divJ = app.CreateList(",", null, .0045);
         divJ.SetBackColor("#4c4b4a");
         var btns = ["[fa-copy]", "[fa-cut]", "[fa-paste]"];
         for (var i = 0; i < btns.length; i++) {
            btn = app.CreateButton(btns[i], .108, null, "FontAwesome");
            btn.icon = btns[i];
            btn.SetTextSize(9);
            btn.SetOnTouch(keys_OnTouch);
            layDrawerClipBarA.AddChild(btn);
            layDrawerClipBarA.SetBackColor("#2c2b2a");
            layDrawerClipBarA.SetBackAlpha(.63);
         }
         var btns2 = ["[fa-share-alt]", "[fa-times-circle]", "[fa-arrows-alt]"]
         for (var i = 0; i < btns2.length; i++) {
            btn = app.CreateButton(btns2[i], .108, null, "FontAwesome");
            btn.icon = btns2[i];
            btn.SetTextSize(9);
            btn.SetOnTouch(keys_OnTouch);
            layDrawerClipBarB.AddChild(btn);
            layDrawerClipBarB.SetBackColor("#2c2b2a");
            layDrawerClipBarB.SetBackAlpha(.63);
         }
         var labeltext = app.CreateList("[fa-text-height]", .108, null, "fontawesome");
         labeltext.SetEnabled(false)
         layTxtSize = app.CreateLayout("Linear", "Horizontal");
         layTxtSize.SetGravity("VCenter");
         layTxtSize.AddChild(labeltext);
         labeltext.SetTextSize(9);
         listSpin = app.CreateList("");
         listSpin.SetSize(.108, null);
         var n = 1;
         do {
            listSpin.AddItem(n);
            n += 1
         } while (n < 90)
         spnTxtSize = uix.CreatePicker(listSpin.GetList(","), .108, null, "NoCycle")
         spnTxtSize.SetTextSize(9)
         spnTxtSize.SetOnChange(spnTxtChange);
         layTxtSize.AddChild(spnTxtSize);
         layDlg = app.CreateDialog("dlg", "NoDim");
         laySettings = app.CreateLayout("linear");
         dlgSettings = app.CreateDialog("Settings", "NoDim");
         dlgSettings.AddLayout(laySettings);
         listSettings = app.CreateList("Favorite1,favorite2,favorite3,favoriten");
         laySettings.AddChild(listSettings);
         /*
         dlgTidySettings = app.CreateDialog("Tidy Settings"  );
         layTidy = app.CreateLayout( "Linear", "VCenter");
         dlgTidySettings.AddLayout( layTidy );
         */
         /*
         txtTidy = app.CreateTextEdit( (JSON.stringify(opt)), .45,.54,"multiline,nospell" );	
         btnTidyStart= app.CreateButton( "Tidy Code" );
         layTidy.AddChild( txtTidy );
         layTidy.AddChild( btnTidyStart );

         btnTidyStart.SetOnTouch( function ()
         {
          

         var opt = txtTidy.GetText()
         */
         btnTidy = app.CreateButton("[fa-cubes]" + "\n Tidy", null, null, "fontawesome")
         btnTidy.SetTextSize(9);
         btnTidy.SetOnTouch(function() {
            var opt = {
               "indent_size": 3,
               "indent_char": " ",
               "indent_level": 0,
               "indent_with_tabs": false,
               "preserve_newlines": true,
               "max_preserve_newlines": 1,
               "jslint_happy": false,
               "space_after_named_function": false,
               "space_after_anon_function": false,
               "brace_style": "collapse",
               "keep_array_indentation": false,
               "keep_function_indentation": false,
               "space_before_conditional": true,
               "break_chained_methods": false,
               "eval_code": false,
               "unescape_strings": false,
               "wrap_line_length": 0
            }
            app.CloseDrawer("right");
            txtUpdate.SetText("tidying")
            dlgpp.Show()
            edtTxt.SetText(js_beautify(edtTxt.GetText(), opt))
            dlgpp.Dismiss()
            edtTxt.Focus()
         });
         btnSettings = app.CreateButton("[fa-cogs] \n Settings", null, null, "fontawesome")
         btnSettings.SetTextSize(9);
         btnSettings.SetOnTouch(function() {
            app.SaveBoolean("assetsExt", false, "assetsExt.txt");
            opts.extract()
         });
         btnME = app.CreateButton("[fa-clone]", .108, null, "fontawesome");
         btnME.SetTextSize(9);
         btnME.SetOnTouch(function() {
            dlgmult.Show();
         });
         layTriggers = app.CreateLayout("Linear", "horizontal");
         layTriggers.AddChild(btnME);
         btnPdf = app.CreateButton("[fa-file-pdf-o]", .108, null, "fontawesome");
         btnPdf.SetTextSize(9);
         btnPdf.SetOnTouch(function() {
            app.OpenDrawer("left")
               //app.SimulateTouch(drawer.layMain, .71, .05, "down");
            drawer.tabs.ShowTab(" PDF Viewer")
         });
         layTriggers.AddChild(btnPdf);
         togTheme = app.CreateSpinner("Default,Dark,Light", -1, -1, "Center");
         togTheme.SetBackColor("#2c2b2a");
         togTheme.SetTextColor("silver");
         togTheme.SetOnChange(function(checked) {
            switch (togTheme.GetText()) {
               case "Light":
                  edtTxt.SetColorScheme("light")
                  edtTxt.SetBackAlpha(.45)
                  edtTxt.SetBackColor("silver");
                  break;
               case "Default":
                  edtTxt.SetColorScheme("dark");
                  edtTxt.SetBackColor("#783873");
                  edtTxt.SetBackAlpha(.09)
                  break;
               case "Dark":
                  edtTxt.SetColorScheme("light");
                  edtTxt.SetBackAlpha(.45)
                  edtTxt.SetBackColor("black", "red", "green")
                  edtTxt.SetTextColor("white")
                  break;
            }
         });
         btnKeyBarConfig = app.CreateButton("⚙️⌨️⚙️", .216, null, "fontawesome");
         btnKeyBarConfig.SetOnTouch(function() {
            keybar.KeyEditor();
         });
         btnDocs = app.CreateButton("Docs", .216, null);
         btnDocs.SetOnTouch(function() {
            docs.show()
         });
         togAutoC = app.CreateToggle("AutoC", .216, null);
         autoOnCheck = app.LoadBoolean("AutoOn", false, "AutoOn.txt")
         switch (autoOnCheck) {
            case true:
               togAutoC.SetChecked(true);
               break;
            case false:
               togAutoC.SetChecked(false);
               break;
         }
         togAutoC.SetOnTouch(function(checked) {
            this.checked = checked
            switch (this.checked) {
               case true:
                  app.SaveBoolean("AutoOn", true, "AutoOn.txt");
                  break;
               case false:
                  app.SaveBoolean("AutoOn", false, "AutoOn.txt");
                  break;
            }
         });
         layDrawer = app.CreateLayout("Linear", "Vertical");
         layDrawer.AddChild(divE);
         layHClip = app.CreateLayout("Linear", "Vertical");
         layHClip.AddChild(divA);
         layHClip.AddChild(layTxtSize);
         layHClip.AddChild(divB);
         layHClip.AddChild(layTriggers);
         layHClip.AddChild(divC);
         layDrawerClipBarC.AddChild(layDrawerClipBarA);
         layDrawerClipBarC.AddChild(layDrawerClipBarB);
         layDrawer.AddChild(layDrawerClipBarC);
         layDrawer.AddChild(layHClip);
         layDrawer.AddChild(btnSettings);
         layDrawer.AddChild(btnTidy)
         layDrawer.AddChild(divF)
         layDrawer.AddChild(togTheme);
         layDrawer.AddChild(divD)
         layDrawer.AddChild(btnKeyBarConfig);
         layDrawer.AddChild(divG)
         layDrawer.AddChild(btnDocs);
         layDrawer.AddChild(divH)
         layDrawer.AddChild(togAutoC);
         layDrawer.AddChild(divJ);
         layDrawer.SetBackColor("#2c2b2a");
         return layDrawer
            //app.AddDrawer(layDrawer, "right", .216, .0108);
      }
   } //end.clipBar
function spnTxtChange(item) {
      edtTxt.SetTextSize(item);
   } //end.spnTxtChange