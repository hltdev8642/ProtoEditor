//------------------------------------------------------------------------
//audioPlayer.js 
//------------------------------------------------------------------------
function audioPlayer() {
      audioPlayer.fileName = this.fileName || "";
      audioPlayer.format = this.format || "";
      audioPlayer.type = this.type || "";
      this.CreateAudioPlayer = function() {
            audioPlayer.mediaplayer = app.CreateMediaPlayer();
            audioPlayer.mediaplayer.SetFile(audioPlayer.fileName);
            audioPlayer.layoutControls = app.CreateLayout("linear", "Horizontal,FillXY");
            audioPlayer.layoutControls.SetGravity("HCenter");
            audioPlayer.mainLayout = app.CreateLayout("linear", "Vertical");
            audioPlayer.btnReset = app.CreateButton("[fa-step-backward]", -1, -1, "FontAwesome");
            audioPlayer.btnReset.SetOnTouch(function() {
               audioPlayer.btnstouch("reset")
            });
            audioPlayer.btnRewind = app.CreateButton("[fa-backward]", -1, -1, "FontAwesome");
            audioPlayer.btnRewind.SetOnTouch(function() {
               audioPlayer.btnstouch("rewind")
            });
            audioPlayer.btnPlay = app.CreateButton("[fa-play]", -1, -1, "FontAwesome");
            audioPlayer.btnPlay.SetOnTouch(function() {
               audioPlayer.btnstouch("play")
            });
            audioPlayer.btnPause = app.CreateButton("[fa-pause]", -1, -1, "FontAwesome");
            audioPlayer.btnPause.SetOnTouch(function() {
               audioPlayer.btnstouch("pause")
            });
            audioPlayer.btnStop = app.CreateButton("[fa-stop]", -1, -1, "FontAwesome");
            audioPlayer.btnStop.SetOnTouch(audioPlayer.btnsontouch);
            audioPlayer.btnStop.SetOnTouch(function() {
               audioPlayer.btnstouch("stop")
            });
            audioPlayer.btnFastForward = app.CreateButton("[fa-forward]", -1, -1, "FontAwesome");
            audioPlayer.btnFastForward.SetOnTouch(function() {
               audioPlayer.btnstouch("forward")
            });
            audioPlayer.btn = app.CreateButton("[fa-forward]", -1, -1, "FontAwesome");
            audioPlayer.btnFastForward.SetOnTouch(function() {
               audioPlayer.btnstouch("forward")
            });
            audioPlayer.btnFastForward = app.CreateButton("[fa-forward]", -1, -1, "FontAwesome");
            audioPlayer.btnFastForward.SetOnTouch(function() {
               audioPlayer.btnstouch("forward")
            });
            audioPlayer.btnLoad = app.CreateButton("[fa-folder-open]", -1, -1, "FontAwesome");
            audioPlayer.btnLoad.SetOnTouch(function() {
               audioPlayer.btnstouch("load");
            });
            audioPlayer.duration = audioPlayer.mediaplayer.GetDuration()
            audioPlayer.barSeek = app.CreateSeekBar(1, -1);
            audioPlayer.barSeek.SetRange(1.0);
            audioPlayer.barSeek.SetOnTouch(function() {
               audioPlayer.btnstouch("seek");
            });
            audioPlayer.trackInfoLabel = app.CreateList("Track Info:");
            audioPlayer.trackInfoLabel.SetEnabled(false)
            audioPlayer.trackInfo = app.CreateText("");
            audioPlayer.trackInfo.SetTextSize(14.4)
            audioPlayer.trackFormatLabel = app.CreateList("Track Format:");
            audioPlayer.trackFormatLabel.SetEnabled(false);
            audioPlayer.trackFormat = app.CreateText("");
            audioPlayer.trackFormat.SetTextSize(14.4)
            audioPlayer.layoutControls.AddChild(audioPlayer.btnReset);
            audioPlayer.layoutControls.AddChild(audioPlayer.btnRewind);
            audioPlayer.layoutControls.AddChild(audioPlayer.btnPlay);
            audioPlayer.layoutControls.AddChild(audioPlayer.btnPause);
            audioPlayer.layoutControls.AddChild(audioPlayer.btnStop);
            audioPlayer.layoutControls.AddChild(audioPlayer.btnFastForward);
            audioPlayer.mainLayout.AddChild(audioPlayer.layoutControls);
            audioPlayer.layoutControls.AddChild(audioPlayer.btnLoad);
            audioPlayer.mainLayout.AddChild(audioPlayer.barSeek);
            audioPlayer.mainLayout.AddChild(audioPlayer.trackInfoLabel);
            audioPlayer.mainLayout.AddChild(audioPlayer.trackInfo);
            audioPlayer.mainLayout.AddChild(audioPlayer.trackFormatLabel);
            audioPlayer.mainLayout.AddChild(audioPlayer.trackFormat)
            audioPlayer.dialog = app.CreateDialog("", "NoTitle");
            audioPlayer.dialog.SetOnBack(function() {
               audioPlayer.dialog.Dismiss();
               audioPlayer.mediaplayer.Stop();
               audioPlayer.dialog.Gone();
            });
            audioPlayer.dialog.AddLayout(audioPlayer.mainLayout);
         } //end audioPlayer.CreateAudioPlayer
      this.Show = function() {
            audioPlayer.dialog.Show();
         } //end audioPlayer.Show
      audioPlayer.update = function() {
            if (audioPlayer.mediaplayer.GetDuration()) {
               audioPlayer.barSeek.SetValue(audioPlayer.mediaplayer.GetPosition() / audioPlayer.mediaplayer.GetDuration());
            }
         } //end audioPlayer.update
      this.LoadAudio = function(fileName) {
            audioPlayer.mediaplayer.SetFile(fileName);
            var trackFormat = fileName.substring(fileName.lastIndexOf(".") + 1)
            var trackName = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.lastIndexOf("."))
            audioPlayer.trackInfo.SetText(trackName);
            audioPlayer.trackFormat.SetText(trackFormat);
            audioPlayer.mediaplayer.Play();
         } //end audioPlayer.LoadAudio
      this.PlayAudio = function() {
            audioPlayer.mediaplayer.Play();
         } //end audioPlayer.PlayAudio
      audioPlayer.btnstouch = function(title) {
            switch (title) {
               case "reset":
                  audioPlayer.mediaplayer.SeekTo(0);
                  break;
               case "rewind":
                  audioPlayer.mediaplayer.SeekTo(audioPlayer.mediaplayer.GetPosition() - 30);
                  break;
               case "play":
                  audioPlayer.mediaplayer.Play();
                  break;
               case "pause":
                  audioPlayer.mediaplayer.Pause();
                  break;
               case "stop":
                  audioPlayer.mediaplayer.Stop();
                  break;
               case "forward":
                  audioPlayer.mediaplayer.SeekTo(audioPlayer.mediaplayer.GetPosition() + 30);
                  break;
               case "load":
                  app.ChooseFile(null, "", function(fileName) {
                     audioPlayer.mediaplayer.SetFile(fileName);
                     var trackFormat = fileName.substring(fileName.lastIndexOf(".") + 1)
                     var trackName = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.lastIndexOf("."))
                     audioPlayer.trackInfo.SetText(trackName);
                     audioPlayer.trackFormat.SetText(trackFormat);
                     audioPlayer.mediaplayer.Play();
                  })
                  break;
               case "seek":
                  audioPlayer.mediaplayer.SeekTo(audioPlayer.mediaplayer.GetDuration() * audioPlayer.barSeek.GetValue());
                  setInterval("audioPlayer.update()", 1000);
                  break;
            } //end.switch
         } //end audioPlayer.btnstouch
   } //audioPlayer.end 
//------------------------------------------------------------------------
//videoPlayer.js 
//------------------------------------------------------------------------
function videoPlayer() {
      videoPlayer.fileName = this.fileName || ""
      videoPlayer.format = this.format || ""
      videoPlayer.type = this.type || ""
      this.LoadVideo = function(fileName) {
         this.fileName = fileName || videoPlayer.fileName
         videoPlayer.mediaplayer.SetFile(this.fileName)
      }
      this.PlayVideo = function() {
         videoPlayer.mediaplayer.Play();
      }
      this.CreateVideoPlayer = function() {
            videoPlayer.mediaplayer = app.CreateVideoView(1, -1);
            videoPlayer.mediaplayer.SetFile(videoPlayer.fileName);
            videoPlayer.layoutControls = app.CreateLayout("linear", "Horizontal,FillXY");
            videoPlayer.layoutControls.SetGravity("HCenter");
            videoPlayer.mainLayout = app.CreateLayout("linear", "Vertical");
            videoPlayer.btnReset = app.CreateButton("[fa-step-backward]", -1, -1, "FontAwesome");
            videoPlayer.btnReset.SetOnTouch(function() {
               videoPlayer.btnstouch("reset")
            });
            videoPlayer.btnRewind = app.CreateButton("[fa-backward]", -1, -1, "FontAwesome");
            videoPlayer.btnRewind.SetOnTouch(function() {
               videoPlayer.btnstouch("rewind")
            });
            videoPlayer.btnPlay = app.CreateButton("[fa-play]", -1, -1, "FontAwesome");
            videoPlayer.btnPlay.SetOnTouch(function() {
               videoPlayer.btnstouch("play")
            });
            videoPlayer.btnPause = app.CreateButton("[fa-pause]", -1, -1, "FontAwesome");
            videoPlayer.btnPause.SetOnTouch(function() {
               videoPlayer.btnstouch("pause")
            });
            videoPlayer.btnStop = app.CreateButton("[fa-stop]", -1, -1, "FontAwesome");
            videoPlayer.btnStop.SetOnTouch(videoPlayer.btnsontouch);
            videoPlayer.btnStop.SetOnTouch(function() {
               videoPlayer.btnstouch("stop")
            });
            videoPlayer.btnFastForward = app.CreateButton("[fa-forward]", -1, -1, "FontAwesome");
            videoPlayer.btnFastForward.SetOnTouch(function() {
               videoPlayer.btnstouch("forward")
            });
            videoPlayer.btn = app.CreateButton("[fa-forward]", -1, -1, "FontAwesome");
            videoPlayer.btnFastForward.SetOnTouch(function() {
               videoPlayer.btnstouch("forward")
            });
            videoPlayer.btnFastForward = app.CreateButton("[fa-forward]", -1, -1, "FontAwesome");
            videoPlayer.btnFastForward.SetOnTouch(function() {
               videoPlayer.btnstouch("forward")
            });
            videoPlayer.btnLoad = app.CreateButton("[fa-folder-open]", -1, -1, "FontAwesome");
            videoPlayer.btnLoad.SetOnTouch(function() {
               videoPlayer.btnstouch("load");
            });
            videoPlayer.duration = videoPlayer.mediaplayer.GetDuration()
            videoPlayer.barSeek = app.CreateSeekBar(1, -1);
            videoPlayer.barSeek.SetRange(1.0);
            videoPlayer.barSeek.SetOnTouch(function() {
               videoPlayer.btnstouch("seek");
            });
            videoPlayer.layoutControls.AddChild(videoPlayer.btnRewind);
            videoPlayer.layoutControls.AddChild(videoPlayer.btnPlay);
            videoPlayer.layoutControls.AddChild(videoPlayer.btnPause);
            videoPlayer.layoutControls.AddChild(videoPlayer.btnStop);
            videoPlayer.layoutControls.AddChild(videoPlayer.btnFastForward);
            videoPlayer.mainLayout.AddChild(videoPlayer.layoutControls);
            videoPlayer.layoutControls.AddChild(videoPlayer.btnLoad);
            videoPlayer.mainLayout.AddChild(videoPlayer.barSeek);
            videoPlayer.mainLayout.AddChild(videoPlayer.mediaplayer);
            videoPlayer.dialog = app.CreateDialog("", "NoTitle");
            videoPlayer.dialog.AddLayout(videoPlayer.mainLayout);
            videoPlayer.dialog.SetOnCancel(function() {
               videoPlayer.mediaplayer.Stop();
               videoPlayer.mediaplayer.Gone();
            });
            videoPlayer.Show()
         } //end videoPlayer.CreatwVideoPlayer
      videoPlayer.Show = function() {
            videoPlayer.dialog.Show();
         } //end videoPlayer.Show
      videoPlayer.GetPlayer = function() {
            return videoPlayer.mainLayout();
         } //end videoPlayer.GetPlayer
      videoPlayer.update = function() {
            if (videoPlayer.mediaplayer.GetDuration()) {
               videoPlayer.barSeek.SetValue(videoPlayer.mediaplayer.GetPosition() / videoPlayer.mediaplayer.GetDuration());
            }
         } //end videoPlayer.update
      videoPlayer.btnstouch = function(title) {
            switch (title) {
               case "reset":
                  videoPlayer.mediaplayer.SeekTo(0);
                  break;
               case "rewind":
                  videoPlayer.mediaplayer.SeekTo(videoPlayer.mediaplayer.GetPosition() - 5);
                  break;
               case "play":
                  videoPlayer.mediaplayer.Play();
                  break;
               case "pause":
                  videoPlayer.mediaplayer.Pause();
                  break;
               case "stop":
                  videoPlayer.mediaplayer.Stop();
                  break;
               case "forward":
                  videoPlayer.mediaplayer.SeekTo(videoPlayer.mediaplayer.GetPosition() + 5);
                  break;
               case "load":
                  app.ChooseFile(null, "", function(fileName) {
                     videoPlayer.mediaplayer.SetFile(fileName);
                     videoPlayer.mediaplayer.Play();
                  })
                  break;
               case "seek":
                  videoPlayer.mediaplayer.SeekTo(videoPlayer.mediaplayer.GetDuration() * videoPlayer.barSeek.GetValue());
                  setInterval("videoPlayer.update()", 1000);
                  break;
            } //end.switch
         } //end videoPlayer.btnstouch
   } //videoPlayer.end
//------------------------------------------------------------------------
//browser.js 
//------------------------------------------------------------------------
function browser() {
      browser.build = function() {
         app.CloseDrawer("left");
         browser.addressbar = app.CreateTextEdit("http://www.google.com", 1, null, "singleline");
         browser.addressbar.SetOnEnter(function() {
            if (browser.addressbar.GetText().substring(0, 4) != "http") {
               browser.addressbar.SetText("http://" + browser.addressbar.GetText());
            }
            if (browser.addressbar.GetText().indexOf(".") <= -1) {
               browser.addressbar.SetText(browser.addressbar.GetText() + ".com");
            }
            browser.webview.LoadUrl(browser.addressbar.GetText().replace(" ", "%20"));
         });
         browser.addressbar.SetBackColor("#CC783873");
         browser.laywebb = app.CreateLayout("frame");
         browser.laywebb.SetSize(1, 1);
         browser.laywebb.SetBackColor("#CC783873");
         browser.laywebb.SetBackAlpha(.91);
         browser.layBrowse = app.CreateLayout("Linear", "Vertical");
         browser.webview = app.CreateWebView(1, .81, "IgnoreErrors,AllowZoom,AutoZoom");
         browser.webview.SetBackAlpha(.81);
         browser.webview.SetOnProgress(function() {
            browser.addressbar.SetText(browser.webview.GetUrl());
         });
         browser.laywebb.AddChild(browser.layBrowse);
         browser.btnWebExit = app.CreateButton("[fa-close] Exit", -1, .082, "FontAwesome");
         browser.btnWebExit.SetBackColor("#CCC22B2A");
         browser.btnWebGo = app.CreateButton("[fa-first-order] Go", -1, .082, "FontAwesome");
         browser.btnWebGo.SetBackColor("#CC2CB22A");
         browser.btnWebReload = app.CreateButton("[fa-refresh] Reload", -1, .082, "FontAwesome");
         browser.btnWebReload.SetBackColor("#CCC2B22A");
         browser.btnWebForward = app.CreateButton("[fa-arrow-right] Forward", -1, .082, "FontAwesome");
         browser.btnWebForward.SetBackColor("#CC2c2ba2")
         browser.btnWebBack = app.CreateButton("[fa-arrow-left] Back", -1, .082, "FontAwesome");
         browser.btnWebBack.SetBackColor("#CC2c2ba2");
         browser.layAddBar = app.CreateLayout("Linear", "Horizontal");
         browser.layAddBar.SetGravity("Center");
         browser.layAddBar.AddChild(browser.btnWebBack);
         browser.layAddBar.AddChild(browser.btnWebReload);
         browser.layAddBar.AddChild(browser.btnWebGo);
         browser.layAddBar.AddChild(browser.btnWebForward);
         browser.layAddBar.AddChild(browser.btnWebExit);
         browser.divWebTop = app.CreateList(" ", 1, .0045);
         browser.divWebTop.SetBackColor("#4c4b4a");
         browser.layBrowse.AddChild(browser.divWebTop);
         browser.layBrowse.AddChild(browser.layAddBar);
         browser.divWebMid = app.CreateList(" ", 1, .0045);
         browser.divWebMid.SetBackColor("#4c4b4a");
         browser.layBrowse.AddChild(browser.divWebMid);
         browser.btnWebGo.SetOnTouch(function() {
            browser.webview.LoadUrl(browser.addressbar.GetText());
         });
         browser.btnWebReload.SetOnTouch(function() {
            browser.webview.Reload();
         });
         browser.btnWebForward.SetOnTouch(function() {
            browser.webview.Forward();;
         });
         browser.btnWebBack.SetOnTouch(function() {
            browser.webview.Back();;
         });
         browser.btnWebExit.SetOnTouch(function() {
            browser.laywebb.Gone();
         });
         browser.layBrowse.AddChild(browser.addressbar);
         browser.divWebBottom = app.CreateList(" ", 1, .0045);
         browser.divWebBottom.SetBackColor("#4c4b4a");
         browser.layBrowse.AddChild(browser.divWebBottom);
         browser.webview.LoadUrl("http://www.google.com ");
         browser.layBrowse.AddChild(browser.webview)
         browser.btnWebPrintB = app.CreateButton("[fa-print]", 1, -1, "fontawesome,graybutton");
         browser.btnWebPrintB.SetOnTouch(function() {
            browser.webview.Print();
         });
         browser.layBrowse.AddChild(browser.btnWebPrintB);
         browser.divWebEnd = app.CreateList(",", 1, .0045);
         browser.divWebEnd.SetBackColor("#4c4b4a");
         browser.laywebb.AddChild(browser.divWebEnd);
         app.AddLayout(browser.laywebb);
         browser.laywebb.Animate("SlideFromTop");
         browser.laywebb.Hide();
      }
      browser.LoadPage = function(url) {
         browser.url = url;
         browser.webview.LoadUrl(browser.url);
         //	browser.webview.SetOnProgress(browser.progress);
         browser.addressbar.SetText(browser.url);
         browser.laywebb.Animate("SlideFromTop");
      }
   } //end.browser