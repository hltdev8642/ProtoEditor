//------------------------------------------------------------------------
//editorBarB
//------------------------------------------------------------------------
function editorBarB() {
      this.init = function(barBH) {
         editorBarB.edtBarBackColor = "#2c2b2a";
         editorBarB.h = barBH || -1
         editorBarB.edtBar = app.CreateLayout("Linear", "horizontal");
         editorBarB.edtBar.SetBackColor(editorBarB.edtBarBackColor);
         editorBarB.edtBar.SetSize(1, editorBarB.h);
         var globalTextSize = 12.2
         app.SaveBoolean("isUndo", false);
         btnUndoFast = app.CreateText("[fa-backward]", 1 / 9, -1, "fontawesome")
         btnUndoFast.SetOnTouchDown(function() {
                  app.LockDrawer( "left" );
            app.SaveBoolean("isUndo", true);
            setInterval(function() {
               if (app.LoadBoolean("isUndo") == true) {
                  edtTxt.Undo();
               }
            }, 180)
         });
         btnUndoFast.SetOnTouchUp(function() {
            app.SaveBoolean("isUndo", false);
               app.UnlockDrawer( "left" );
         });
         editorBarB.edtBar.AddChild(btnUndoFast);
         btnUndo = app.CreateList("[fa-undo]", 1 / 9, -1, "FontAwesome");
         btnUndo.icon = "[fa-undo]"
         btnUndo.SetOnTouch(keys_OnTouch);
         editorBarB.edtBar.AddChild(btnUndo);
         btnSearch = app.CreateList("[fa-search]", 1 / 9, -1, "FontAwesome");
         btnSearch.icon = "[fa-search]";
         btnSearch.SetOnTouch(keys_OnTouch);
         editorBarB.edtBar.AddChild(btnSearch);
         searchF = app.CreateTextEdit("", 1.5 / 9, -1, "singleline");
         searchF.SetHint("find");
         searchF.SetOnTouch(function() {
            app.SaveBoolean("isSearch", true);
         });
         searchF.SetTextSize(globalTextSize);
         searchF.SetOnEnter(function() {
            edtTxt.Search(searchF.GetText());
         });
         editorBarB.edtBar.AddChild(searchF);
         searchR = app.CreateTextEdit("", 1.5 / 9, -1, "singleline");
         searchR.SetHint("replace");
         searchR.SetOnTouch(function() {
            app.SaveBoolean("isSearch", true);
         });
         searchR.SetOnEnter(function() {
            edtTxt.Search(searchF.GetText());
            edtTxt.Replace(searchR.GetText());
         });
         searchR.SetTextSize(globalTextSize);
         editorBarB.edtBar.AddChild(searchR);
         btnReplace = app.CreateList("[fa-exchange]", 1 / 9, -1, "FontAwesome");
         btnReplace.icon = "[fa-exchange]";
         btnReplace.SetOnTouch(keys_OnTouch);
         btnReplace.SetOnLongTouch(keys_OnLTouch);
         editorBarB.edtBar.AddChild(btnReplace);
         btnRepeat = app.CreateList("[fa-repeat]", 1 / 9, -1, "FontAwesome");
         btnRepeat.icon = "[fa-repeat]";
         btnRepeat.SetOnTouch(keys_OnTouch);
         editorBarB.edtBar.AddChild(btnRepeat);
         btnRepeatFast = app.CreateText("[fa-forward]", 1 / 9, -1, "button,fontawesome")
         btnRepeatFast.SetOnTouchDown(function() {
         app.LockDrawer( "right" );
            app.SaveBoolean("isRedo", true);
            setInterval(function() {
               if (app.LoadBoolean("isRedo") == true) {
                  edtTxt.Redo();
               }
            }, 180)
         } );
         
         btnRepeatFast.SetOnTouchUp(function() {
            btnRepeatFast.SetBackColor("#2c2b2a");
            app.SaveBoolean("isRedo", false);
            app.UnlockDrawer( "right" );
         });
         editorBarB.edtBar.AddChild(btnRepeatFast);
         return editorBarB.edtBar
      }
   } //end.editorBarB