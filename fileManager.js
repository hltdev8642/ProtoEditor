//------------------------------------------------------------------------
//fileMgr.js 
//------------------------------------------------------------------------
function openFile() {
      app.ChooseFile("Choose a file", "\*/\*", onFileChoose)
   } //end.openFile
function osr() {
      osr.url = function(txtIn) {
            osr.file = txtIn
               //url check 
            var regexUrl = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/ig
            var isURL = regexUrl.exec(osr.file)
               //alert(isURL!==null)
            if (isURL) {
               browser.LoadPage(osr.file)
               return true;
            }
         } //osr.url
      osr.status = function(txtIn) {
            osr.file = txtIn
            var regexUrl = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/ig
            var isURL = regexUrl.exec(osr.file)
            return isURL;
         } //osr.status
   } //end osr
function onFileChoose(file) {
      app.SaveText("currentFile", file, "currentFile.txt");
      titleBar.SetPath(app.LoadText("currentFile", null, "currentFile.txt"));
      drawer.SetPath(app.LoadText("currentFile", null, "currentFile.txt"));
      if ((file.indexOf(".mp3") > -1) | (file.indexOf(".flac") > -1) | (file.indexOf(".wav") > -1) | (file.indexOf(".aac") > -1) | (file.indexOf(".wma") > -1)) {
         var ap = new audioPlayer();
         app.SaveText("currentFile", file, "currentFile.txt");
         listRecents.AddItem(file, 'audio', null);
         app.SaveBoolean("isProtected", true, "isProtected.txt");
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         spnSyntax.SetText("audio");
         ap.CreateAudioPlayer()
         ap.LoadAudio(file);
         ap.Show()
         ap.PlayAudio()
         return
      }
      if ((file.indexOf("mp4") > -1) | (file.indexOf(".mov") > -1) | (file.indexOf(".avi") > -1) | (file.indexOf(".mkv") > -1) | (file.indexOf(".mpeg4") > -1) | (file.indexOf(".3gp") > -1) | (file.indexOf(".webm") > -1)) {
         var vp = new videoPlayer();
         app.SaveText("currentFile", file, "currentFile.txt");
         app.SaveBoolean("isProtected", true, "isProtected.txt");
         listRecents.AddItem(file, 'video', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         spnSyntax.SetText("video");
         vp.CreateVideoPlayer()
         vp.LoadVideo(file);
         vp.PlayVideo();
         return
      }
      if ((file.indexOf(".jpg") > -1) | (file.indexOf(".png") > -1) | (file.indexOf(".jpeg") > -1) | (file.indexOf(".svg") > -1) | (file.indexOf(".psd") > -1) | (file.indexOf(".bmp") > -1) | (file.indexOf(".gif") > -1) | (file.indexOf(".ico") > -1) | (file.indexOf(".cr2") > -1) | (file.indexOf(".raw") > -1)) {
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText("image");
         app.SaveBoolean("isProtected", true, "isProtected.txt");
         webImg = app.CreateWebView(null, null, "IgnoreErrors,NoScrollBars,AllowZoom")
         layImg = app.CreateLayout("frame");
         layImg.AddChild(webImg);
         layTr = app.CreateLayout("linear", "Vertical,Right");
         layImg.AddChild(layTr);
         layTr.SetPadding(null, .18, null, null);
         btnClose = app.CreateButton("[fa-close]", -1, null, "FontAwesome");
         btnShare = app.CreateButton("[fa-share-alt]", -1, null, "FontAwesome");
         btnPrint = app.CreateButton("[fa-print]", -1, null, "FontAwesome");
         layTr.AddChild(btnClose);
         layTr.AddChild(btnShare);
         layTr.AddChild(btnPrint);
         btnPrint.SetOnTouch(function() {
            webImg.Print();
         });
         btnShare.SetOnTouch(function() {
            app.SendFile("file://" + file)
         });
         btnClose.SetOnTouch(function() {
            layM.SetTouchable(true);
            layImg.Gone();
         });
         app.AddLayout(layImg);
         var mdContent2 = '![](' + file + ')';
         var md2 = new mdParser(file, mdContent2);
         var html2 = md2.GetHtml();
         webImg.ClearHistory();
         webImg.LoadHtml(html2);
         layM.SetTouchable(false);
         layImg.Animate("SlideFromBottom");
         spnSyntax.SetText('Syntax');
         listRecents.AddItem(file, "image");
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt");
         return;
      }
      if (file.indexOf(".pdf") > -1) {
         webtmp = app.CreateWebView(1, 1);
         //dlgtmp = app.CreateDialog("","NoDim");
         //dlgtmp.AddLayout(webtmp);
         scriptA = app.CreateText(" ");
         scriptA.SetText(app.ReadFile('jsA.txt'));
         scriptB = app.CreateText(" ");
         scriptB.SetText(app.ReadFile('jsB.txt'));
         // app.DeleteFile('web/viewer.js', );
         app.SaveText("currentFile", file, "currentFile.txt");
         listRecents.AddItem(file, '.pdf ', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         spnSyntax.SetText("pdf");
         app.SaveBoolean("isProtected", true, "isProtected.txt");
         app.WriteFile(pathData.GetText() + "/web/viewer.js", scriptA.GetText() + file + scriptB.GetText());
         //app.WriteFile("web/viewer.js", app.LoadText("jsA", null, "jsA.txt") + edtPath.GetText() + app.LoadText("jsB", null, "jsB.txt"));
         drawer.webpdf.Hide()
         drawer.webpdf.Show()
            //drawer webpdf.ClearFocus();
         drawer.webpdf.Reload();
         drawer.webpdf.ClearHistory();
         drawer.webpdf.LoadUrl("file:///" + pathData.GetText() + "/web/viewer.html");
         //webtmp.LoadUrl("file:///sdcard/documents/hlteditor/data/web/viewer.html");
         app.OpenDrawer("left");
         //app.SimulateTouch(drawer.layMain, .71, .05, "down");
         drawer.tabs.ShowTab(" PDF Viewer")
            //webtmp.SetOnProgress(function() {
         app.OpenDrawer("left");
         //});
         return;
      }
      if (file.indexOf(".py") > -1) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText('python');
         listRecents.AddItem(file, ".py", null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return
      }
      if (file.indexOf(".php") > -1) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText('php');
         listRecents.AddItem(file, '.php', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      }
      if ((file.indexOf(".txt") > -1) | (file.indexOf(".text") > -1) | (file.indexOf(".dat") > -1)) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText('text');
         listRecents.AddItem(file, '.txt', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      }
      if (file.indexOf(".js") > -1) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText('javascript');
         listRecents.AddItem(file, '.js', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      }
      if (file.indexOf(".rb") > -1) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText('ruby');
         listRecents.AddItem(file, '.rb', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      }
      if (file.indexOf(".c") > -1) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText(".c");
         listRecents.AddItem(file, '.c', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      }
      if (file.indexOf(".h") > -1) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText(".c");
         listRecents.AddItem(file, '.h', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt");
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      }
      if (file.indexOf(".ino") > -1) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText(".c");
         listRecents.AddItem(file, '.ino', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      }
      if (file.indexOf(".pde") > -1) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText('.java');
         listRecents.AddItem(file, '.pde', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      }
      if (file.indexOf(".md") > -1) {
         if (isEdtA == true) {
            edtA.SetText(app.ReadFile(file));
            return;
         }
         if (isEdtB == true) {
            edtB.SetText(app.ReadFile(file));
            return;
         }
         edtTxt.SetText(app.ReadFile(file));
         app.SaveText("currentFile", file, "currentFile.txt");
         spnSyntax.SetText('markdown');
         listRecents.AddItem(file, '.md', null);
         app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      } else {
         dlgOpen = app.CreateDialog("Open File", "NoDim");
         optA = app.CreateButton("Open With ...");
         optA.SetOnTouch(function() {
            var file = app.LoadText("currentFile", null, "currentFile.txt");
            listRecents.AddItem(file, 'other');
            app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
            app.OpenFile(file, "*/*");
            app.SaveText("currentFile", "", "currentFile.txt");
         });
         optB = app.CreateButton("Open");
         optB.SetOnTouch(function() {
            var file = app.LoadText("currentFile", null, "currentFile.txt");
            edtTxt.SetText(app.ReadFile(file));
            app.SaveText("currentFile", file, "currentFile.txt");
            spnSyntax.SetText('Syntax');
            listRecents.AddItem(file, "other");
            app.SaveText("listRecents", listRecents.GetList(","), "listRecents.txt")
            dlgOpen.Hide();
         });
         layDlgOpen = app.CreateLayout("Linear", "VCenter");
         layDlgOpen.AddChild(optA);
         layDlgOpen.AddChild(optB);
         dlgOpen.AddLayout(layDlgOpen);
         dlgOpen.Show();
         app.SaveBoolean("isProtected", false, "isProtected.txt");
         return;
      }
   } //end.onFileChoose