function docs() {
      docs.build = function() {
         lay = app.CreateLayout("linear", "VTop,FillXY,touchthrough");
         layBtn = app.CreateLayout("linear", "VBottom,fillxy,touchthrough");
         btnClose = app.CreateButton("[fa-close]", 1, .09, "gray,fontawesome");
         btnClose.SetOnTouch(function() {
            docs.hide()
         });
         frame = app.CreateLayout("frame");
         layBtn.AddChild(btnClose);
         view = app.CreateWebView(1, .91, "IgnoreErrors,allowremote");
        view.LoadUrl("file:///sdcard/Droidscript/.edit/docs/docs.htm");
       // view.LoadHtml( app.ReadFile( "file:///sdcard/Droidscript/.edit/docs/docs.htm" ))
         lay.AddChild(view);
         layBtn.Hide();
         lay.Hide();
         app.AddLayout(layBtn);
         app.AddLayout(lay);
      }
      docs.show = function() {
         layBtn.Animate("slidefrombottom");
         lay.Animate("slidefromtop");
      }
      docs.hide = function() {
         lay.Animate("slidetotop");
         layBtn.Animate("slidetobottom");
      }
   } //end.docs