//------------------------------------------------------------------------
//multiEdit.js 
//------------------------------------------------------------------------
function multiEdit() {
      this.init = function() {
         layA = app.CreateLayout("linear", "VCenter,fillx");
         edtA = app.CreateCodeEdit("isEdtA", 1, .36);
         edtA.SetOnDoubleTap(function() {
            edtA.Paste()
         });
         edtB = app.CreateCodeEdit("isEdtB", 1, .36);
         edtB.SetLanguage("markdown")
         edtB.SetOnDoubleTap(function() {
            edtB.Paste()
         });
         barO = app.CreateLayout("linear", "horizontal,fillx");
         barO.SetBackColor("#2c2b2a");
         btnInsertA = app.CreateButton("insertA");
         btnInsertA.SetOnTouch(function() {
            edtTxt.InsertText(edtA.GetText())
         });
         btnInsertB = app.CreateButton("insertB");
         btnInsertB.SetOnTouch(function() {
            edtTxt.InsertText(edtB.GetText())
         });
         btnA = app.CreateButton("openA");
         btnA.SetOnTouch(function() {
            var oldFile = app.LoadText("currentFile", null, "currentFile.txt");
            app.SaveText("lastFile", oldFile, "lastFile.txt");
            isEdtA = true
            isEdtB = false
            openFile()
         });
         btnB = app.CreateButton("openB");
         btnB.SetOnTouch(function() {
            var oldFile = app.LoadText("currentFile", null, "currentFile.txt");
            app.SaveText("lastFile", oldFile, "lastFile.txt");
            isEdtA = false
            isEdtB = true
            openFile()
         });
         barO.AddChild(btnInsertA);
         barO.AddChild(btnA);
         barO.AddChild(btnB);
         barO.AddChild(btnInsertB);
         layA.AddChild(edtA);
         layA.AddChild(barO);
         layA.AddChild(edtB);
         return layA
      }
   } //end.multiEdit