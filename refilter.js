function refilter() {
      edtTxt.SetOnDoubleTap(function() {
         app.OpenDrawer("right");
      });
      /*enable for key shortcut commandoutput*/
      /*edtTxt.SetOnChange(function (title) {*/
      /*gets current key*/ //
      var txttmp = edtTxt.GetText().substring(edtTxt.GetCursorPos(), edtTxt.GetLineStart(edtTxt.GetCursorLine()))
      if (txttmp.indexOf('\"') > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf('\"') + 1), (edtTxt.GetCursorPos())))
      } else if (txttmp.indexOf(",") > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf(",") + 1), (edtTxt.GetCursorPos())))
      } else if (txttmp.indexOf(".") > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf(".") + 1), (edtTxt.GetCursorPos())))
      } else if (txttmp.indexOf("function ") > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf(" ") + 1), (edtTxt.GetCursorPos())))
      } else if (txttmp.indexOf("=") > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf("=") + 1), (edtTxt.GetCursorPos())))
      } else if (txttmp.indexOf(",") > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf(",") + 1), (edtTxt.GetCursorPos())))
      } else if (txttmp.indexOf("if") > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf("if") + 1), (edtTxt.GetCursorPos())))
      } else if (txttmp.indexOf("for") > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf("for") + 1), (edtTxt.GetCursorPos())))
      } else if (txttmp.indexOf("do") > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf("do") + 1), (edtTxt.GetCursorPos())))
      } else if (txttmp.indexOf("<") > -1) {
         filterButtons(mybuttons, edtTxt.GetText().substring((edtTxt.GetText().lastIndexOf("<") + 1), (edtTxt.GetCursorPos())))
      } else {}
   } //end.refilter
function findableButton(array, text, width, height, options) {
      var btn = app.CreateList(text, null, null, "")
      btn.SetTextSize(12.6)
      btn.SetOnTouch(function(title) {
         var edtTmp = edtTxt.GetText().substring(edtTxt.GetText().lastIndexOf(".") + 1, edtTxt.GetCursorPos())
         var txttmp = edtTxt.GetText().substring(edtTxt.GetCursorPos(), edtTxt.GetLineStart(edtTxt.GetCursorLine()))
         if (txttmp.indexOf('\"') > -1) {
            edtTxt.ReplaceText(title, edtTxt.GetText().lastIndexOf('\"'), edtTxt.GetCursorPos())
         } else if (txttmp.indexOf(',') > -1) {
            edtTxt.ReplaceText(title, edtTxt.GetText().lastIndexOf(','), edtTxt.GetCursorPos())
         } else if (txttmp.indexOf(".") > -1) {
            edtTxt.ReplaceText(title, edtTxt.GetText().lastIndexOf(".") + 1, edtTxt.GetCursorPos())
         } else if (txttmp.indexOf("function ") > -1) {
            edtTxt.ReplaceText(title, edtTxt.GetText().lastIndexOf(" ") + 1, edtTxt.GetCursorPos())
         } else if (txttmp.indexOf(",") > -1) {
            edtTxt.ReplaceText(title, edtTxt.GetText().lastIndexOf(",") + 1, edtTxt.GetCursorPos())
         } else if (txttmp.indexOf("if") > -1) {
            edtTxt.ReplaceText(title, edtTxt.GetText().lastIndexOf("if"), edtTxt.GetCursorPos())
         } else if (txttmp.indexOf("for") > -1) {
            edtTxt.ReplaceText(title, edtTxt.GetText().lastIndexOf("for"), edtTxt.GetCursorPos())
         } else if (txttmp.indexOf("do") > -1) {
            edtTxt.ReplaceText(title, edtTxt.GetText().lastIndexOf("do"), edtTxt.GetCursorPos())
         } else if (txttmp.indexOf("<") > -1) {
            edtTxt.ReplaceText(title, edtTxt.GetText().lastIndexOf("<"), edtTxt.GetCursorPos())
         } else {}
         // edtTxt.ReplaceText(edtTxt.Search(title,"down",true,true),edtTxt.GetCursorPos(), edtTxt.Search(".")
      })
      btn.text = text;
      array.push(btn);
      return btn
   } //end.findableButton
function filterButtons(array, filter) {
      var btns;
      for (var key in array) {
         btns = array[key];
         if ((btns.text.toLowerCase()).indexOf(filter.toLowerCase()) > -1) {
            //btn.ScrollToItem(btns.text, null, scrKeys)
            btns.Show()
         } else {
            btns.Gone()
         }
      }
   } //end.filterButtons