//------------------------------------------------------------------------
//run.js 
//------------------------------------------------------------------------
function run(syntax) {
      switch (syntax) {
         case "Syntax:":
            app.SaveBoolean("isProtected", false, "isProtected.txt");
            break;
         case "markdown":
            var md = new mdParser("ProtoEditor", edtTxt.GetText());
            var html = md.GetHtml();
            web.ClearHistory();
            web.LoadHtml(html);
            webd.Show();
            break;
         case "quicksand":
            var txt = edtTxt.GetText();
            var fileTmp = app.LoadText("pathDat", null, "pathDat.txt") + "/" + "tmp.js"
            app.WriteFile(fileTmp, edtTxt.GetText());
            app.StartApp(fileTmp);
            break;
         case "javascript":
            var txt = edtTxt.GetText();
            var fileJs = app.LoadText("currentFile", null, "currentFile.txt");
            if (!app.LoadBoolean("isDebug")) {
               app.StartApp(fileJs);
            }
            if (app.LoadBoolean("isDebug")) {
               app.StartApp(fileJs, "debug,overlay");
            }
            break;
         case "python":
            var filePy = app.LoadText("currentFile", null, "currentFile.txt");
            app.OpenFile(filePy, ".py");
            break;
         case "html":
            var txt = edtTxt.GetText();
            web.ClearHistory();
            web.LoadHtml(edtTxt.GetText());
            webd.Show()
            break;
         case "php":
            app.SaveBoolean("isProtected", false, "isProtected.txt");
            var txt = edtTxt.GetText();
            web.ClearHistory();
            web.LoadHtml(edtTxt.GetText());
            webd.Show()
            break;
         case "image":
            app.SaveBoolean("isProtected", true, "isProtected.txt");
            var imgTmp = app.LoadText("currentFile", null, "currentFile.txt");
            app.OpenFile(imgTmp, "image");
            break;
         case "video":
            app.SaveBoolean("isProtected", true, "isProtected.txt");
            var vidTmp = app.LoadText("currentFile", null, "currentFile.txt");
            app.OpenFile(vidTmp, "video");
            break;
         case "audio":
            app.SaveBoolean("isProtected", true, "isProtected.txt");
            var audTmp = app.LoadText("currentFile", null, "currentFile.txt");
            app.OpenFile(audTmp, "audio");
            break;
         case "pdf":
            app.SaveBoolean("isProtected", true, "isProtected.txt");
            var pdfTmp = app.LoadText("currentFile", null, "currentFile.txt");
            app.OpenFile(pdfTmp, "document");
            break;
      } //end switch
      return;
   } //end.run