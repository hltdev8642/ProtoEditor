//------------------------------------------------------------------------
//extraKeyBar.js 
//------------------------------------------------------------------------
function extraKeyBar() {
      this.backColor = "#783863"
      this.globalAlpha = .18
      this.textSize = 12.6
      this.init = function(kbHeight) {
            extraKeyBar.h = kbHeight || null
            layKeys = app.CreateLayout("linear", "horizontal");
            layKeys.SetBackColor(this.backColor);
            layKeys.SetBackAlpha(this.globalAlpha);
            frameKeys = app.CreateScroller(1, extraKeyBar.h, "NoScrollBars");
            frameKeys.AddChild(layKeys);
            scrKeys = app.CreateLayout("Linear", "Horizontal");
            frameKeys.SetMargins(0, 0, 0, 0);
            frameKeys.SetPadding(0, 0, 0, 0);
            //scrKeys.SetSize(1, extraKeyBar.h);
            scrKeys.SetBackColor("#2c2b2a");
            scrKeys.AddChild(frameKeys);
         } //end extraKeyBar.init()
      this.SetKeys = function(keys) {
            this.keys = keys;
            btns = [];
            spns = [];
         } //end extraKeyBar.setKeys()
      this.GetExtraKeyBar = function(editor) {
            this.editor = editor
            var keys = this.keys;
            chlist = app.CreateList("")
            var keysplit = keys.split(/\r\n|\r|\n/gm)
            for (i = 0; i < keysplit.length; i++) {
               var ch = keysplit[i];
         
                  chlist.AddItem(keysplit[i]);
           //       btn = app.CreateList(ch);
                  btn = findableButton(mybuttons, ch, -1, extraKeyBar.h);
                  btn.SetOnLongTouch(function(title, body, index) {
                  
                   editor.InsertText(title);
                  });
                  layKeys.AddChild(btn);
                  // btns.push(btn);
         
            } //endfor
            return scrKeys
         } //end extraKeyBar.GetBar()
      extraKeyBar.KeyEditorConfig = function(wsPath) {
            extraKeyBar.path = wsPath || app.LoadText("pathDat", "", "pathDat.txt");
            extraKeyBar.layout = app.CreateLayout("Linear", "Top,FillXY");
            extraKeyBar.editor = app.CreateTextEdit("", 0.90, 0.36);
            extraKeyBar.editor.SetText(app.ReadFile(extraKeyBar.path + '/keys.txt'));
            extraKeyBar.btnSetKeys = app.CreateButton("Set Keys");
            extraKeyBar.btnSetKeys.SetOnTouch(function() {
               app.WriteFile(extraKeyBar.path + '/keys.txt', extraKeyBar.editor.GetText());
               alert("Keys set, restart app to initialize changes")
               extraKeyBar.dialog.Dismiss();
               app.CloseDrawer("right");
            });
            extraKeyBar.dialog = app.CreateDialog('KeyBar Editor');
            extraKeyBar.dialog.SetBackColor( "#2c2b2a" );
             extraKeyBar.dialog.SetBackAlpha( 0.18 );
            extraKeyBar.layout.AddChild(extraKeyBar.editor);
            extraKeyBar.layout.AddChild(extraKeyBar.btnSetKeys);
            extraKeyBar.dialog.AddLayout(extraKeyBar.layout);
         } //end extraKeyBar.KeyEditor()
      this.KeyEditor = function() {
            extraKeyBar.KeyEditorConfig(app.LoadText('pathDat', null, 'pathDat.txt'));
            extraKeyBar.dialog.SetOnCancel(function() {
               app.CloseDrawer("right");
            });
            extraKeyBar.dialog.SetOnBack(function() {
               app.CloseDrawer("right");
            });
            extraKeyBar.dialog.Show();
         } //end extraKeyBar.KeyEditor
   } //end.extraKeyBar